<?php

return [
    'production' => true,
    'baseUrl' => '',
    'title' => 'Jigsaw',
    'description' => 'Example Jigsaw site using GitLab Pages',
    'collections' => [],
];
